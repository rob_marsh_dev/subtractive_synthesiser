#include <stdlib.h>		//standard library for memory allocation, process control, coversions etc
#include <stdio.h>		//standard library for managing inputs and outputs
#include <string.h>		//functions for manipulating strings and arrays
#include <math.h>		//functions for computing mathmatical operations and transformations

#include "SynthesizerDefinitions.h"
#include "WaveTable.h"
SynthConfig synthConfig;

void main()
{
	int pitch = 48;		//48 = Middle C
	float duration = 2;
	float volume = 1;

	//SynthConfig synthConfig;
	FrequencyValue frequency = synthConfig.GetFrequency(pitch);
	
	PhaseAccumulator phaseIncrement = synthConfig.frequencyRadians * frequency;
	PhaseAccumulator phase = 0;

	long totalSamples = (long)((synthConfig.sampleRate * duration));
}
