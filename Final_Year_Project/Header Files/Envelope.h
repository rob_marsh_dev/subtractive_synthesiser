#include "SubtractiveSynthesiserEnums.h"

class Envelope {
	public:
		Envelope();
		~Envelope();

		void SetEnvelopeState(EnvelopeState);
		void SetAttackTime(float);
		void SetAttackLevel(float);
		void SetDecayTime(float);
		void SetSustainLevel(float);
		void SetReleaseTime(float);

		void InitialiseEnvelope(float, float, float, float, float);
		float ApplyEnvelope();
		void Reset();

	private:
		float Index;
		double Amplitude;
		EnvelopeState State;
		float AttackTime;
		float AttackLevel;
		float DecayTime;
		float SustainLevel;
		float ReleaseTime;
};