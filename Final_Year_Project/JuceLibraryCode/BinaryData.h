/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#ifndef BINARYDATA_H_92754774_INCLUDED
#define BINARYDATA_H_92754774_INCLUDED

namespace BinaryData
{
    extern const char*   power_button_png;
    const int            power_button_pngSize = 2391;

    extern const char*   rotary_knob_png;
    const int            rotary_knob_pngSize = 1504;

    extern const char*   logo_png;
    const int            logo_pngSize = 16925;

    extern const char*   logo_without_text_png;
    const int            logo_without_text_pngSize = 271354;

    extern const char*   sine_png;
    const int            sine_pngSize = 7706;

    extern const char*   sawtooth_png;
    const int            sawtooth_pngSize = 7655;

    extern const char*   square_png;
    const int            square_pngSize = 7156;

    extern const char*   triangle_png;
    const int            triangle_pngSize = 7679;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Number of elements in the namedResourceList array.
    const int namedResourceListSize = 8;

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes) throw();
}

#endif
