#include <string>
#include <vector>
#include "SubtractiveSynthesiserEnums.h"

#pragma once
class Oscillator
{
public:
	Oscillator();
	~Oscillator();

	//Getters and setters
	bool GetState();
	WaveformTypes GetWaveformType();
	int GetWavetableLength();
	double GetWavetableIndex();
	std::vector<double> GetWavetable();
	double GetWavetableElement(int);
	float GetAmplitude();
	int GetPitch();

	void ToggleState();
	void SetOscillator(WaveformTypes);
	void SetWaveformType(WaveformTypes);
	void SetWavetableLength(int);
	void SetWavetableIndex(double);
	void SetWavetable(WaveformTypes);
	void SetAmplitude(float);
	void SetPitch(int);

private:
	bool State;
	WaveformTypes WaveformType;
	int WavetableLength = 2048;
	double WavetableIndex;
	std::vector<double> Wavetable;
	float Amplitude;
	int Pitch;
};

