#include "audioeffectx.h"

#define NUM_PARAMS 0

class VSTProject : public AudioEffectX
{
public:
	VSTProject(audioMasterCallback audioMaster);
	~VSTProject();

	void ProcessReplacing(float **inputs, float ** outputs, VstInt32 sampleFrames);
};

