#pragma once
class BiQuadFilter
{
public:
	BiQuadFilter();
	~BiQuadFilter();

	//Getters and setters
	double GetCutoffFrequency();
	double GetResonance();
	void SetCutoffFrequency(double);
	void SetResonance(double);

	virtual void CalculateCoefficient();
	double ApplyFilter(double);

protected:
	double cutoffFrequency;
	double resonance;
	double ampIn0;
	double ampIn1;
	double ampIn2;
	double ampOut1;
	double ampOut2;
	double delayIn1;
	double delayIn2;
	double delayOut1;
	double delayOut2;
};

class LowPassFilter : public BiQuadFilter
{
	void CalculateCoefficient();
};

class HighPassFilter : public BiQuadFilter
{
	void CalculateCoefficient();
};

