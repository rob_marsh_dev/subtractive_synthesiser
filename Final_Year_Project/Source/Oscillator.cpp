#include "../Header Files/Oscillator.h"

Oscillator::Oscillator()
{
	//Default values for oscillator = sine waveform
	State = 1;
	WaveformType = Sine;
	WavetableIndex = 0;
	SetWavetable(WaveformType);
	Amplitude = 1.0;
	Pitch = 0;
}

Oscillator::~Oscillator()
{
}

bool Oscillator::GetState()
{
	return State;
}

WaveformTypes Oscillator::GetWaveformType()
{
	return WaveformType;
}

int Oscillator::GetWavetableLength()
{
	return WavetableLength;
}

double Oscillator::GetWavetableIndex() 
{
	return WavetableIndex;
}

std::vector<double> Oscillator::GetWavetable()
{
	return Wavetable;
}

double Oscillator::GetWavetableElement(int element)
{
	if (element < Wavetable.size())
		return Wavetable[element];
	else
		return 0;
}

float Oscillator::GetAmplitude()
{
	return Amplitude;
}

int Oscillator::GetPitch()
{
	return Pitch;
}

void Oscillator::ToggleState() 
{
	State = !State;
}

void Oscillator::SetOscillator(WaveformTypes type)
{
	WaveformType = type;
	SetWavetable(type);
	Amplitude = 1.0;
	Pitch = 0;
}

void Oscillator::SetWaveformType(WaveformTypes type)
{
	WaveformType = type;
}

void Oscillator::SetWavetableLength(int length)
{
	WavetableLength = length;
}

void Oscillator::SetWavetableIndex(double index)
{
	WavetableIndex = index;
}

void Oscillator::SetWavetable(WaveformTypes type)
{	
	Wavetable.clear();
	double phase = 0;
	double phaseIncrement;
	const double  double_Pi = 3.1415926535897932384626433832795;

	switch (type) {
		case Square:
			for (int i = 0; i < WavetableLength; i++)
			{
				if (i < WavetableLength / 2)
					Wavetable.push_back(1.0);
				else
					Wavetable.push_back(0.0);
			}
			break;
		case Triangle:
			phaseIncrement = 4.0 / WavetableLength;
			for (int i = 0; i < WavetableLength; i++)
			{
				Wavetable.push_back(phase);
				if ((phase += phaseIncrement) > 1.0)
				{
					phase -= 2 * phaseIncrement;
					phaseIncrement = -phaseIncrement;
				}
				else if (phase < -1.0)
				{
					phase -= 2 * phaseIncrement;
					phaseIncrement = -phaseIncrement;
				}
			}
			break;
		case Sawtooth:
			phaseIncrement = 2.0 / WavetableLength;
			phase = 0;
			for (int i = 0; i < WavetableLength; i++)
			{
				Wavetable.push_back(phase);
				phase += phaseIncrement;

				if (phase >= 1.0)
					phase -= 2.0;
			}
			break;
		default:			//sine wave
			phaseIncrement = (2 * double_Pi) / WavetableLength;
			for (int i = 0; i < WavetableLength; i++)
			{
				Wavetable.push_back(float(sin(phase)));
				phase += phaseIncrement;
			}
			break;
	}
}

void Oscillator::SetAmplitude(float amplitude)
{
	Amplitude = amplitude;
}

void Oscillator::SetPitch(int pitch)
{
	Pitch = pitch;
}

