#ifndef _SYNTHESIZERDEFINITIONS_H_
#define _SYNTHESIZERDEFINITIONS_H_

// sample output type
typedef short SampleValue;
// 8-bit data type
typedef char bsInt8;
// 16-bit data type
typedef short bsInt16;
// 32-bit data type
typedef int bsInt32;
// 8-bit unsigned type
typedef unsigned char bsUint8;
// 16-bit unsigned type 
typedef unsigned short bsUint16;
// 32-but unsigned type
typedef unsigned int bsUint32;

typedef double PhaseAccumulator;
typedef float AmplitudeValue;
typedef double HighPrecisionAmplitudeValue;
typedef float FrequencyValue;

// PI values
#define PI 3.14159265358979
#define twoPI 6.28318530717958

class SynthConfig
{
public:
	FrequencyValue sampleRate;
	// maximum frequency (also known as Nyquist limit)
	FrequencyValue maximumFrequency;
	// multiplier to convert internal sample values to output values
	AmplitudeValue sampleScale;
	// pre-calculated multiplier for frequency to radians (twoPI/sampleRate)
	PhaseAccumulator frequencyRadians;
	// pre-calculated multiplier for frequency to table index (tableLength/sampleRate) 
	PhaseAccumulator frequencyTableIndex; 
	// pre-calculated multiplier for radians to table index (tableLength/twoPI)
	PhaseAccumulator radiansTableIndex;
	PhaseAccumulator waveTableLength;
	// maximum phase increment for wavetables (waveTableLength/2)
	PhaseAccumulator maximumPhaseIncrement;
	// table to convert pitch index into frequency
	FrequencyValue tuning[128];
	// table to convert cents +/-1200 into frequency multiplier
	FrequencyValue cents[2401];	

	SynthConfig()
	{
		Initialise();
		
		size_t sampleBits = (sizeof(SampleValue) * 8) - 1;
		sampleScale = (AmplitudeValue)((1 << sampleBits) - 1);

		// this process generates the frequencies of each note
		double frequency = 13.75 * pow(2.0, 0.25);
		double frequencyMultiplier = pow(2.0, 1.0/12.0);
		for (int i = 0; i < 128; i++)
		{
			tuning[i] = (FrequencyValue)frequency;
			frequency = frequency * frequencyMultiplier;
		}

		frequency = -1200;
		for (int i = 0; i < 2401; i++)
		{
			cents[i] = (FrequencyValue) pow(2.0, frequency/1200.0);
			frequency += 1.0;
		}
	}

	// Function serves as a constructor but can be called directly to alter the sample 
	// rate and table length
	void Initialise(bsInt32 _sampleRate = 44100, bsInt32 _tableLength = 16384)
	{
		sampleRate = (FrequencyValue)_sampleRate;
		maximumFrequency = sampleRate / 2;
		waveTableLength = (PhaseAccumulator)_tableLength;
		maximumPhaseIncrement = waveTableLength / 2;
		frequencyRadians = twoPI / (PhaseAccumulator)sampleRate;
		frequencyTableIndex = waveTableLength / (PhaseAccumulator)sampleRate;
		radiansTableIndex = waveTableLength / twoPI;
	}

	FrequencyValue GetFrequency(int pitch)
	{
		return tuning[pitch];
	}
};

#endif