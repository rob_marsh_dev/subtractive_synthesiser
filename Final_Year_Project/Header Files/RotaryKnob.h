#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

class RotaryKnob : public Slider
{
	public:
		void paint(Graphics& g)
		{
			setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
			const juce::Image knobImage = ImageCache::getFromMemory(BinaryData::rotary_knob_png, BinaryData::rotary_knob_pngSize);
			g.drawImageAt(knobImage, 0, 0, false);
		}
};

//class FilmStripKnob : public Slider
//{
//public:
//	FilmStripKnob(File const& image, const int numFrames, const bool stripIsHorizontal)
//		: Slider(image.getFullPathName() + (" FilmStripKnob")),
//		filmStrip(image.exists() ? ImageFileFormat::loadFrom(image) : 0),
//		numFrames_(numFrames),
//		isHorizontal_(stripIsHorizontal)
//	{
//		if (filmStrip)
//		{
//			setTextBoxStyle(NoTextBox, 0, 0, 0);
//
//			if (isHorizontal_) {
//				frameHeight = filmStrip->getHeight();
//				frameWidth = filmStrip->getWidth() / numFrames_;
//			}
//			else {
//				frameHeight = filmStrip->getHeight() / numFrames_;
//				frameWidth = filmStrip->getWidth();
//			}
//		}
//	}
//
//	~FilmStripKnob() { delete filmStrip; }
//
//	void paint(Graphics& g)
//	{
//		if (filmStrip) {
//			int value = (getValue() - getMinimum()) / (getMaximum() - getMinimum()) * (numFrames_ - 1);
//			if (isHorizontal_) {
//				g.drawImage(filmStrip, 0, 0, getWidth(), getHeight(),
//					value * frameWidth, 0, frameWidth, frameHeight);
//			}
//			else {
//				g.drawImage(filmStrip, 0, 0, getWidth(), getHeight(),
//					0, value * frameHeight, frameWidth, frameHeight);
//			}
//		}
//
//	}
//
//	int getFrameWidth() const { return filmStrip ? frameWidth : 100; }
//	int getFrameHeight() const { return filmStrip ? frameHeight : 24; }
//
//private:
//	Image* filmStrip;
//	const int numFrames_;
//	const bool isHorizontal_;
//	int frameWidth, frameHeight;
//};
