#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../Header Files/Oscillator.h"
#include "../Header Files/BiQuadFilter.h"
#include "../Header Files/RotaryKnob.h"
#include "../Header Files/Envelope.h"
#include "../JuceLibraryCode/BinaryData.h"
#include "../Header Files/Components.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent : public AudioAppComponent,
							 private ComboBox::Listener,
						  	 private MidiInputCallback,
							 private MidiKeyboardStateListener,
							 private Slider::Listener
{
public:
    //==============================================================================
	MainContentComponent()
		: SampleRate(0.0),
		LastMidiInputIndex(0),
		CurrentKey(0),
		keyboardComponent(keyboardState, MidiKeyboardComponent::horizontalKeyboard),
		OscillatorTabs(TabbedButtonBar::Orientation::TabsAtLeft)
    {
		// TODO - Ideally move this to Initialise method
		for (int i = 0; i < numberOfOscillators; i++)
		{
			OscillatorComponent* oc = new OscillatorComponent();
			OscillatorComponents[i] = oc;
			OscillatorTabs.addTab("OSC" + std::to_string(i + 1), Colours::aliceblue, OscillatorComponents[i], false, i + 1);
		}
		
		addAndMakeVisible(midiInputList);
		addAndMakeVisible(keyboardComponent);

		midiInputList.setTextWhenNoChoicesAvailable("No MIDI inputs available");
		const StringArray midiDevices = MidiInput::getDevices();
		midiInputList.addItemList(midiDevices, 1);

		for (int i = 0; i < midiDevices.size(); ++i)
			if (deviceManager.isMidiInputEnabled(midiDevices[i]))
			{
				SetMidiInput(i, midiDevices);
				break;
			}

		if (midiInputList.getSelectedId() == 0)
			SetMidiInput(0, midiDevices);
		midiInputList.addListener(this);

		keyboardState.addListener(this);
		setSize (800, 300);
		
		volumeSlider.setRange(0, 0.25);
		volumeSlider.setValue(0.125, dontSendNotification);
		volumeSlider.setTextBoxStyle(volumeSlider.NoTextBox, true, 0, 0);
		addAndMakeVisible(volumeSlider);

		filterCutoffSlider.setRange(1, 10000);
		filterCutoffSlider.setValue(1000, dontSendNotification);
		filterCutoffSlider.addListener(this);
		addAndMakeVisible(filterCutoffSlider);
		filterTypeList.addItem("Lowpass", 1);
		filterTypeList.addItem("Highpass", 2);
		filterTypeList.setSelectedId(1, dontSendNotification);
		addAndMakeVisible(filterTypeList);
		filterTypeList.addListener(this);
		filter = new LowPassFilter;
		filter->CalculateCoefficient();

		//addAndMakeVisible(testKnob);
		/*testKnob.addListener(this);
		testKnob.setRange(0, 1);*/

		attackTimeSlider.setRange(0, 3, 0.1);
		attackTimeSlider.addListener(this);
		attackLevelSlider.setRange(0, 1, 0.05);
		attackLevelSlider.setValue(1, dontSendNotification);
		attackLevelSlider.addListener(this);
		decayTimeSlider.setRange(0, 3, 0.1);
		decayTimeSlider.addListener(this);
		sustainLevelSlider.setRange(0, 1, 0.05);
		sustainLevelSlider.setValue(1, dontSendNotification);
		sustainLevelSlider.addListener(this);
		releaseTimeSlider.setRange(0, 3, 0.1);
		releaseTimeSlider.addListener(this);
		addAndMakeVisible(attackTimeSlider);
		addAndMakeVisible(attackLevelSlider);
		addAndMakeVisible(decayTimeSlider);
		addAndMakeVisible(sustainLevelSlider);
		addAndMakeVisible(releaseTimeSlider);

		addAndMakeVisible(OscillatorTabs);
		OscillatorTabs.setTabBarDepth(20);

		// specify the number of input and output channels that we want to open
		setAudioChannels(0, 2);
    }

    ~MainContentComponent()
    {
        shutdownAudio();
    }

    //=======================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override
    {
        // This function will be called when the audio device is started, or when
        // its settings (i.e. sample rate, block size, etc) are changed.

        // You can use this function to initialise any resources you might need,
        // but be careful - it will be called on the audio thread, not the GUI thread.

        // For more details, see the help for AudioProcessor::prepareToPlay()
		SampleRate = sampleRate;
		double frq = 13.75 * pow(2.0, 0.25);		//C0 = 16.35...
		double two12 = pow(2.0, 1.0 / 12.0); 
		for (int i = 0; i < 127; i++)
		{
			Tuning[i] = (float)frq;
			frq *= two12;
		}

		envelope.InitialiseEnvelope(attackTimeSlider.getValue(), attackLevelSlider.getValue(),
			decayTimeSlider.getValue(), sustainLevelSlider.getValue(), releaseTimeSlider.getValue());
    }

    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
    {
		float audioLevel = (float)volumeSlider.getValue();

		// Get the increment for a single waveform cycle (1Hz)
		// TODO - Get Wavetable length from configurable file rather than oscillator
		double baseIncrement = OscillatorComponents[0]->Oscillator.GetWavetableLength() / SampleRate;

		for (int channel = 0; channel < bufferToFill.buffer->getNumChannels(); ++channel)
		{
			// Get a pointer to the start sample in the buffer for this audio output channel
			float* const buffer = bufferToFill.buffer->getWritePointer(channel, bufferToFill.startSample);

			for (int sample = 0; sample < bufferToFill.numSamples; ++sample)
			{
				if (channel == 0)
				{
					double finalSample = 0;
					for (int i = 0; i < numberOfOscillators; i++)
					{
						int indexTruncated = floor(OscillatorComponents[i]->Oscillator.GetWavetableIndex());
						double indexFractional = OscillatorComponents[i]->Oscillator.GetWavetableIndex() - indexTruncated;
						double indexIncrement = baseIncrement * GetFrequency(CurrentKey + OscillatorComponents[i]->Oscillator.GetPitch());
						if (OscillatorComponents[i]->Oscillator.GetState())
						{
							double difference = OscillatorComponents[i]->Oscillator.GetWavetableElement(indexTruncated + 1) -
								OscillatorComponents[i]->Oscillator.GetWavetableElement(indexTruncated);
							double interpolatedSample = OscillatorComponents[i]->Oscillator.GetWavetableElement(indexTruncated) + (difference * indexFractional);
							finalSample += interpolatedSample;
						}
						OscillatorComponents[i]->Oscillator.SetWavetableIndex(OscillatorComponents[i]->Oscillator.GetWavetableIndex() + indexIncrement);
						if(OscillatorComponents[i]->Oscillator.GetWavetableIndex() > OscillatorComponents[i]->Oscillator.GetWavetableLength())
							OscillatorComponents[i]->Oscillator.SetWavetableIndex(OscillatorComponents[i]->Oscillator.GetWavetableIndex()
																					- OscillatorComponents[i]->Oscillator.GetWavetableLength());
					}

					//TODO - Normalise the final sample

					finalSample *= envelope.ApplyEnvelope();
					if (filter)
						finalSample = filter->ApplyFilter(finalSample);

					buffer[sample] = finalSample * audioLevel;
				}
				else
				{
					memcpy(buffer, bufferToFill.buffer->getReadPointer(0), bufferToFill.numSamples * sizeof(float));
					break;
				}
			}
		}
		
    }

    void releaseResources() override
    {
        // This will be called when the audio device stops, or when it is being
        // restarted due to a setting change.

        // For more details, see the help for AudioProcessor::releaseResources()
    }

    //=======================================================================
    void paint (Graphics& g) override
    {
		// Get images from binary data
		const juce::Image logo = ImageCache::getFromMemory(BinaryData::logo_png, BinaryData::logo_pngSize);
		
		// Code for drawing to main window
		g.fillAll(Colours::silver);
		g.drawImageAt(logo, 0, 0, false);
		//testKnob.paint(g);
    }

    void resized() override
    {
        // This is called when the MainContentComponent is resized.
        // If you add any child components, this is where you should
        // update their positions.

		int screenWidth = getWidth();

		for (int i = 0; i < numberOfOscillators; i++)
			OscillatorComponents[i]->setBounds(10, 100, 285, 95);
		OscillatorTabs.setBounds(0, 100, OscillatorComponents[0]->getWidth() + OscillatorTabs.getTabBarDepth(), OscillatorComponents[0]->getHeight());

		volumeSlider.setBounds(screenWidth - 200, 10, 200, 20);
		filterCutoffSlider.setBounds(screenWidth - 450, 160, 200, 20);
		attackTimeSlider.setBounds(screenWidth - 200, 40, 200, 20);
		attackLevelSlider.setBounds(screenWidth - 200, 70, 200, 20);
		decayTimeSlider.setBounds(screenWidth - 200, 100, 200, 20);
		sustainLevelSlider.setBounds(screenWidth - 200, 130, 200, 20);
		releaseTimeSlider.setBounds(screenWidth - 200, 160, 200, 20);

		filterTypeList.setBounds(screenWidth - 450, 115, 200, 20);
		midiInputList.setBounds(screenWidth - 440, 10, 200, 20);
		keyboardComponent.setBounds(0, getHeight() - 100, screenWidth, 100);
    }

	void comboBoxChanged(ComboBox* box) override
	{
		const StringArray midiDevices = MidiInput::getDevices();
		if (box == &midiInputList)
			SetMidiInput(midiInputList.getSelectedItemIndex(), midiDevices);
		else if (box == &filterTypeList)
		{ 
			SetFilterType(filterTypeList.getSelectedItemIndex());
			filterCutoffSlider.setValue(filter->GetCutoffFrequency());
		}
	}

	void sliderValueChanged(Slider* slider) override
	{
		if (slider == &filterCutoffSlider)
		{
			filter->SetCutoffFrequency(slider->getValue());
			filter->CalculateCoefficient();
		}
		else if (slider == &attackTimeSlider) {
			envelope.SetAttackTime(slider->getValue());
		}
		else if (slider == &attackLevelSlider) {
			envelope.SetAttackLevel(slider->getValue());
		}
		else if (slider == &decayTimeSlider) {
			envelope.SetDecayTime(slider->getValue());
		}
		else if (slider == &sustainLevelSlider) {
			envelope.SetSustainLevel(slider->getValue());
		}
		else if (slider == &releaseTimeSlider) {
			envelope.SetReleaseTime(slider->getValue());
		}
	}

	void SetMidiInput(int index, StringArray midiDevices)
	{
		deviceManager.removeMidiInputCallback(midiDevices[LastMidiInputIndex], this);
		const String newInputDevice(midiDevices[index]);

		if (!deviceManager.isMidiInputEnabled(newInputDevice))
			deviceManager.setMidiInputEnabled(newInputDevice, true);

		deviceManager.addMidiInputCallback(newInputDevice, this);
		midiInputList.setSelectedId(index + 1, dontSendNotification);

		LastMidiInputIndex = index;
	}

	void SetFilterType(int index)
	{
		switch (index) {
			case 0:
				filter = new LowPassFilter();
				filter->CalculateCoefficient();
				break;
			case 1:
				filter = new HighPassFilter();
				filter->CalculateCoefficient();
				break;
			default:
				break;
		}
	}

	void handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message) override
	{
		keyboardState.processNextMidiEvent(message);
	}

	void handleNoteOn(MidiKeyboardState*, int midiChannel, int midiNoteNumber, float velocity) override
	{
		CurrentKey = midiNoteNumber;
		envelope.Reset();
	}

	void handleNoteOff(MidiKeyboardState*, int midiChannel, int midiNoteNumber, float velocity) override
	{
		if (CurrentKey == midiNoteNumber)
			envelope.SetEnvelopeState(Release);
	}

	double GetFrequency(int noteNumber)
	{
		if (noteNumber >= 0 && noteNumber < 127)
			return Tuning[noteNumber];
		else
			return Tuning[0] * pow(2.0, (double)noteNumber / 12.0);
	}

private:
    //==============================================================================
    // Your private member variables go here...
	AudioDeviceManager deviceManager;
	MidiKeyboardState keyboardState;
	MidiKeyboardComponent keyboardComponent;
	ComboBox midiInputList, filterTypeList;
	Slider volumeSlider, filterCutoffSlider, attackTimeSlider, attackLevelSlider, decayTimeSlider, sustainLevelSlider, releaseTimeSlider;
	//RotaryKnob testKnob;
	// TODO - Try and get these down into one variable
	const int numberOfOscillators = 2;
	OscillatorComponent *OscillatorComponents[2];
	TabbedComponent OscillatorTabs;
	Envelope envelope;
	BiQuadFilter* filter;

	int CurrentKey;
	int LastMidiInputIndex;
	double SampleRate;
	double Tuning[128];
	
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};

// (This function is called by the app startup code to create our main component)
Component* createMainContentComponent()     { return new MainContentComponent(); }


#endif  // MAINCOMPONENT_H_INCLUDED
