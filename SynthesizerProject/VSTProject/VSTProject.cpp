#include "VSTProject.h"

AudioEffect* CreateEffectInstance(audioMasterCallback audioMaster)
{
	return new VSTProject(audioMaster);
}

VSTProject::VSTProject(audioMasterCallback audioMaster) : AudioEffectX(audioMaster, 0, NUM_PARAMS)
{
}

VSTProject::~VSTProject()
{
}

void VSTProject::ProcessReplacing(float **inputs, float** outputs, VstInt32 sampleFrames)
{

}