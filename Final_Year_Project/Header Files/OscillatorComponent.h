#pragma once
class OscillatorComponent : public Component,
							private Button::Listener,
							private Slider::Listener
{
	private:
		ToggleButton PowerButton, SineButton, SquareButton, TriangleButton, SawtoothButton;
		Image SineImage, SquareImage, TriangleImage, SawtoothImage;
		Slider DetuneSlider;
	public:
		Oscillator Oscillator;
		OscillatorComponent()
		{
			addAndMakeVisible(PowerButton);
			addAndMakeVisible(SineButton);
			addAndMakeVisible(SquareButton);
			addAndMakeVisible(TriangleButton);
			addAndMakeVisible(SawtoothButton);
			addAndMakeVisible(DetuneSlider);

			PowerButton.addListener(this);
			SineButton.addListener(this);
			SineButton.setRadioGroupId(1);
			SquareButton.addListener(this);
			SquareButton.setRadioGroupId(1);
			TriangleButton.addListener(this);
			TriangleButton.setRadioGroupId(1);
			SawtoothButton.addListener(this);
			SawtoothButton.setRadioGroupId(1);
			
			DetuneSlider.addListener(this);
			DetuneSlider.setRange(-12, 12, 1);
			DetuneSlider.setSliderStyle(Slider::SliderStyle::LinearVertical);
			DetuneSlider.setTextBoxStyle(DetuneSlider.TextBoxRight, true, 30, 15);

			PowerButton.setToggleState(1, dontSendNotification);
			SineButton.setToggleState(1, dontSendNotification);
		}
		
		void paint(Graphics& g) 
		{
			g.fillAll(Colours::lightslategrey);

			SineImage = ImageCache::getFromMemory(BinaryData::sine_png, BinaryData::sine_pngSize);
			SquareImage = ImageCache::getFromMemory(BinaryData::square_png, BinaryData::square_pngSize);
			TriangleImage = ImageCache::getFromMemory(BinaryData::triangle_png, BinaryData::triangle_pngSize);
			SawtoothImage = ImageCache::getFromMemory(BinaryData::sawtooth_png, BinaryData::sawtooth_pngSize);
			g.drawImageAt(SineImage, 25, 10, false);
			g.drawImageAt(SquareImage, 150, 10, false);
			g.drawImageAt(TriangleImage, 25, 50, false);
			g.drawImageAt(SawtoothImage, 150, 50, false);
		}
		
		void resized()
		{
			PowerButton.setBounds(270, 0, 25, 25);
			SineButton.setBounds(0, 15, 25, 25);
			SquareButton.setBounds(125, 15, 25, 25);
			TriangleButton.setBounds(0, 55, 25, 25);
			SawtoothButton.setBounds(125, 55, 25, 25);
			DetuneSlider.setBounds(235, 10, 60, 75);
		}

		void buttonClicked(Button* button) override
		{
			if (button == &PowerButton)
				Oscillator.ToggleState();
			else if (button == &SineButton)
				Oscillator.SetOscillator(Sine);
			else if (button == &SquareButton)
				Oscillator.SetOscillator(Square);
			else if (button == &TriangleButton)
				Oscillator.SetOscillator(Triangle);
			else if (button == &SawtoothButton)
				Oscillator.SetOscillator(Sawtooth);
		}

		void sliderValueChanged(Slider* slider) override
		{
			Oscillator.SetPitch(slider->getValue());	
		}
};
