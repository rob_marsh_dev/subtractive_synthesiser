#pragma once
enum WaveformTypes {
	Sine = 0,
	Square = 1, 
	Triangle = 2,
	Sawtooth = 3
};

enum EnvelopeState {
	Attack = 0,
	Decay = 1,
	Sustain = 2,
	Release = 3,
	Finished = 4
};
