#include <gtest.h>
#include "../Final_Year_Project/Header Files/Oscillator.h"
#include "../Final_Year_Project/Header Files/SubtractiveSynthesiserEnums.h"

struct OscillatorTest : testing::Test {
	Oscillator* oscillator;

	OscillatorTest() {
		oscillator = new Oscillator();
	}

	~OscillatorTest() {
		delete oscillator;
	}
};

TEST_F(OscillatorTest, GetDefaultWaveformType_ReturnsSine) {
	EXPECT_EQ(Sine, oscillator->GetWaveformType());
}

TEST_F(OscillatorTest, SetOscillatorToSquare_ReturnsSquare) {
	oscillator->SetOscillator(Square);
	EXPECT_EQ(Square, oscillator->GetWaveformType());
}

//TEST(Oscillato, SinewaveTest) {
//	Oscillator oscillator;
//	EXPECT_EQ(oscillator.GetAmplitude(), 1);
//}