#include "../Header Files/Envelope.h"

Envelope::Envelope() {
	Index = 0;
	Amplitude = 0;
	State = Attack;
	AttackTime = 1;
	AttackLevel = 1;
	DecayTime = 1;
	SustainLevel = 0.5;
	ReleaseTime = 1;
}

Envelope::~Envelope() {

}

void Envelope::SetAttackTime(float _attackTime) { AttackTime = _attackTime; }
void Envelope::SetAttackLevel(float _attackLevel) { AttackLevel = _attackLevel; }
void Envelope::SetDecayTime(float _decayTime) { DecayTime = _decayTime; }
void Envelope::SetSustainLevel(float _sustainLevel) { SustainLevel = _sustainLevel; }
void Envelope::SetReleaseTime(float _releaseTime) { ReleaseTime = _releaseTime; }

void Envelope::InitialiseEnvelope(float attackTime, float attackLevel,
								  float decayTime, float sustainLevel, float releaseTime)
{
	AttackTime = attackTime;
	AttackLevel = attackLevel;
	DecayTime = decayTime;
	SustainLevel = sustainLevel;
	ReleaseTime = releaseTime;
}

float Envelope::ApplyEnvelope() 
{
	Index++;
	//TODO - Replace 48000 with sample rate from config file
	double attackSample = 48000 * AttackTime;
	double decaySample = 48000 * DecayTime;
	double releaseSample = 48000 * ReleaseTime;

	switch(State)
	{
		case Attack:
			if (Index < attackSample)
				Amplitude += AttackLevel / attackSample;
			else
				State = Decay;
			break;
		case Decay:
			if (Index < attackSample + decaySample)
				Amplitude -= (AttackLevel - SustainLevel) / decaySample;
			else
				State = Sustain;
			break;
		case Sustain:
			Amplitude = SustainLevel;
			break;
		case Release:
			Amplitude -= SustainLevel / releaseSample;
			if (Amplitude <= 0)
			{ 
				Amplitude = 0;
				State = Finished;
			}
			break;
		case Finished:
			return 0;
			break;
		default:
			return 0;
	}

	return Amplitude;
}

void Envelope::SetEnvelopeState(EnvelopeState newState) 
{
	State = newState;
}

void Envelope::Reset() 
{
	Index = 0;
	Amplitude = 0;
	State = Attack;
}