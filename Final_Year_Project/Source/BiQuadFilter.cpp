#include "../Header Files/BiQuadFilter.h"
#include <math.h>

#define squareRoot2 1.414213562
#define PI 3.14159265358979

BiQuadFilter::BiQuadFilter()
{
	//assign default value to filter properties
	cutoffFrequency = 1000;
	resonance = 0;
	ampIn0 = 0;
	ampIn1 = 0;
	ampIn2 = 0;
	ampOut1 = 0;
	ampOut2 = 0;
	delayIn1 = 0;
	delayIn2 = 0;
	delayOut1 = 0;
	delayOut2 = 0;
}

BiQuadFilter::~BiQuadFilter()
{
	
}

double BiQuadFilter::GetCutoffFrequency() { return cutoffFrequency; }
double BiQuadFilter::GetResonance() { return resonance; }
void BiQuadFilter::SetCutoffFrequency(double _cutoffResonance) { cutoffFrequency = _cutoffResonance; }
void BiQuadFilter::SetResonance(double _resonance) { resonance = _resonance; }

void BiQuadFilter::CalculateCoefficient() {}
double BiQuadFilter::ApplyFilter(double inputSample) 
{
	double outputSample = (ampIn0 * inputSample) + (delayIn1 * ampIn1) + (delayIn2 * ampIn2) 
						  - (delayOut1 * ampOut1) - (delayOut2 * ampOut2);
	delayIn2 = delayIn1;
	delayIn1 = inputSample;
	delayOut2 = delayOut1;
	delayOut1 = outputSample;
	return outputSample;
}

void LowPassFilter::CalculateCoefficient() 
{
	if (GetCutoffFrequency() < 1)
		SetCutoffFrequency(1);
	if (GetResonance() < 0.5)
		SetResonance(0.5);
	double c = 1 / tan((PI / 48000) * GetCutoffFrequency());
	double c2 = c * c;
	double csqr2 = squareRoot2 * c;
	double oned = 1.0 / (c2 + csqr2 + 1.0);

	ampIn0 = oned;
	ampIn1 = oned + oned;
	ampIn2 = oned;
	ampOut1 = (2.0 * (1.0 - c2)) * oned;
	ampOut2 = (c2 - csqr2 + 1.0) * oned;
}

void HighPassFilter::CalculateCoefficient()
{
	double c = tan((PI / 48000) * GetCutoffFrequency());
	double c2 = c * c;
	double csqr2 = squareRoot2 * c;
	double oned = 1.0 / (1.0 + c2 + csqr2);

	ampIn0 = oned;     
	ampIn1 = -(ampIn0 + ampIn0);  
	ampIn2 = ampIn0;              
	ampOut1 = ((2.0 * (c2 - 1.0)) * oned);
	ampOut2 = ((1.0 - csqr2 + c2) * oned);
}

